-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.6-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for projekt
CREATE DATABASE IF NOT EXISTS `projekt` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `projekt`;

-- Dumping structure for table projekt.sisendinfo
CREATE TABLE IF NOT EXISTS `sisendinfo` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Year` int(11) NOT NULL,
  `KM` int(11) NOT NULL,
  `Work` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Producer` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Price` int(11) NOT NULL,
  `WorkType` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Notes` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table projekt.sisendinfo: ~131 rows (approximately)
/*!40000 ALTER TABLE `sisendinfo` DISABLE KEYS */;
INSERT INTO `sisendinfo` (`ID`, `Year`, `KM`, `Work`, `Producer`, `Price`, `WorkType`, `Notes`) VALUES
	(3, 2011, 91720, 'Käigukasti sisendvöll', 'OMS', 193, 'Remont', '-\r'),
	(4, 2011, 91720, 'Käigukasti laagrid', 'SKF', 40, 'Remont', '-\r'),
	(5, 2011, 91720, 'Käigukasti tihendid', 'OEM', 40, 'Remont', '-\r'),
	(6, 2011, 91720, 'Õlivahetus filtriga', 'Liqui Moly 20W50 mineraal', 35, 'Hooldus', '-\r'),
	(7, 2011, 91720, 'Ylekande ölid', 'Liqui Moly 75W90', 20, 'Hooldus', '-\r'),
	(8, 2011, 91720, 'Tagapiduri ja siduri vedelik', 'DOT4', 5, 'Hooldus', '-\r'),
	(9, 2011, 91720, 'Kiigelaagrite (pivot) määrimine', 'Lithium määre', 0, 'Hooldus', '-\r'),
	(10, 2012, 91720, 'Tagaklotside vahetus', 'Ferodo ', 30, 'Hooldus', '-\r'),
	(11, 2012, 91720, 'Esiamorditorude simmerid', 'OEM', 50, 'Remont', '-\r'),
	(12, 2012, 91720, 'Gaasitrossid, jagamiskarp, toores', 'OEM', 176, 'Remont', '-\r'),
	(13, 2012, 91720, 'Ajastusketi pinguti, vasak', 'OEM', 60, 'Uuendus', '-\r'),
	(14, 2012, 97500, 'Uued rehvid', 'K60 Scout', 200, 'Hooldus', '-\r'),
	(15, 2012, 98120, 'Õlivahetus filtriga', 'Motorex 10W40 Boxer4t sünteetiline', 35, 'Hooldus', '-\r'),
	(16, 2012, 100445, 'Esiamordid ja sild värvitud', 'Rustoleum', 0, 'Remont', '-\r'),
	(17, 2012, 100445, 'Kuulühenduse uus kumm', 'Kreekast', 0, 'Remont', '-\r'),
	(18, 2012, 100445, 'Esiratta laagrid uued', 'SKT', 0, 'Remont', '-\r'),
	(19, 2012, 100445, 'Suruõhu pump', '-', 0, 'Uuendus', '-\r'),
	(20, 2012, 100445, 'Drosselite ümberehitus', 'Cata Dan', 60, 'Remont', '-\r'),
	(21, 2012, 100445, 'Uued esipidurivoolikud', 'Goodrich', 0, 'Remont', '-\r'),
	(22, 2012, 100445, 'Uus gaasiandur ', 'Ebay kasutatud', 40, 'Remont', '-\r'),
	(23, 2012, 101450, 'Õlivahetus filtriga', 'Liqui Moly 20W50 mineraal', 35, 'Hooldus', '-\r'),
	(24, 2013, 108505, 'Plastikud ja paak värvitud', '-', 180, 'Uuendus', '-\r'),
	(25, 2013, 108505, 'Uued kodarad', '-', 250, 'Uuendus', '-\r'),
	(26, 2013, 108505, 'Veljed ja klapikaaned klaaskuulpritsitud ja pulbervärvitud', '-', 50, 'Uuendus', '-\r'),
	(27, 2013, 108505, 'Sadulanahk uus', 'Sadulsepp Tom', 100, 'Hooldus', '-\r'),
	(28, 2013, 108505, 'Rehvid Dunlop Roadsmart 2', 'Dunlop', 232, 'Hooldus', '-\r'),
	(29, 2013, 108505, 'Kodarad pingutatud (Neeme)', 'Streetmoto', 60, 'Uuendus', '-\r'),
	(30, 2013, 108505, 'Esiklaas poleeritud', 'SmartService', 20, 'Hooldus', '-\r'),
	(31, 2013, 108505, 'Lisatuled ', '(LED + Hella FF75)', 95, 'Uuendus', '-\r'),
	(32, 2013, 108505, 'Esimene ajastuskett koos pingutiga', 'OEM', 59, 'Remont', '-\r'),
	(33, 2013, 108505, 'Kütusefilter', 'OEM', 20, 'Hooldus', '-\r'),
	(34, 2013, 108505, 'Uus tagatuli koos suunaklaasidega', 'Louis.de', 30, 'Uuendus', '-\r'),
	(35, 2013, 108505, 'Õlivahetus filtriga', 'Castrol Act Evo 20W-50', 35, 'Hooldus', '-\r'),
	(36, 2013, 108505, 'Käigukasti sisendvõll OK', '-', 0, 'Hooldus', '-\r'),
	(37, 2013, 108505, 'Taga(suur)laager avatud ja OK', '-', 0, 'Hooldus', '-\r'),
	(38, 2013, 108505, 'Uue tagumise laagri simmerling', 'OEM', 29, 'Hooldus', '-\r'),
	(39, 2013, 110085, 'Uus Hall andur', 'Cata Dan', 150, 'Remont', '-\r'),
	(40, 2014, 113000, 'Uus armatuur', 'OEM', 100, 'Remont', '-\r'),
	(41, 2014, 113000, 'Raamid painutatud', '-', 0, 'Remont', '-\r'),
	(42, 2014, 113000, 'Veljed balansseeritud (olid tglt OK)', 'Streetmoto', 15, 'Remont', '-\r'),
	(43, 2014, 113000, 'Uus esiklaas', 'MRA', 150, 'Remont', '-\r'),
	(44, 2014, 113000, 'Esiklaasi plastjupid', 'OEM', 80, 'Remont', '-\r'),
	(45, 2014, 113000, 'Uued peeglid', 'Poola Ebay', 20, 'Remont', '-\r'),
	(46, 2014, 113000, 'Uus lenks', 'Poola Ebay', 25, 'Remont', '-\r'),
	(47, 2014, 113000, 'Uus lenksulaager', 'SKF', 30, 'Remont', '-\r'),
	(48, 2014, 113000, 'Sildindrikaitsed', 'Steptoe', 233, 'Uuendus', '-\r'),
	(49, 2014, 115800, 'Tagumised ""pivot"" laagrid 2 tk', 'OEM', 116, 'Remont', '-\r'),
	(50, 2014, 115800, 'Kardaani suur laager', 'OEM', 65, 'Hooldus', '-\r'),
	(51, 2014, 115800, 'Kardaani suur simmerling', 'OEM', 29, 'Hooldus', '-\r'),
	(52, 2015, 119890, 'Õlivahetus filtriga', 'Lipoi 20W-50 mineraal', 50, 'Hooldus', '-\r'),
	(53, 2015, 119890, 'Nookuri- ja klapivahed OK', '-', 0, 'Hooldus', '-\r'),
	(54, 2015, 119890, 'Ovaalsed peeglid', 'Ebay', 30, 'Uuendus', '-\r'),
	(55, 2015, 119890, '5v USB pistik', 'Ebay', 15, 'Uuendus', '-\r'),
	(56, 2015, 119890, 'Kardaani ja käimariõli', 'Liqui Moly 80W90', 30, 'Hooldus', '-\r'),
	(57, 2015, 122550, 'Tag. piduriketas', 'TRW', 120, 'Hooldus', '-\r'),
	(58, 2015, 122550, 'Tag. piduriklotsid', 'TRW', 30, 'Hooldus', '-\r'),
	(59, 2015, 125200, 'Uus esi/tagarehv', 'Heidenau K60 Scout', 225, 'Hooldus', '-\r'),
	(60, 2015, 130000, 'Õlivahetus filtriga', 'Castrol 10W-40 poolsünt.', 30, 'Hooldus', '-\r'),
	(61, 2015, 130000, 'Spidotigu', 'OEM', 67, 'Remont', '-\r'),
	(62, 2016, 131258, 'Kompressioon vasak 7,5  bar, parem 5bar', '-', 0, 'Näit', '-\r'),
	(63, 2016, 131258, 'Kolvirõngad 2 kmpl', 'OEM', 182, 'Remont', '-\r'),
	(64, 2016, 131258, 'Väljalaskeklapid (parem) 2tk', 'OEM', 230, 'Remont', '-\r'),
	(65, 2016, 131258, 'Ketitallad (parem ülemine ja vasak ülemine/ alumine)', 'OEM', 40, 'Remont', '-\r'),
	(66, 2016, 131258, 'Klapisääretihendid 8 tk', 'OEM', 35, 'Remont', '-\r'),
	(67, 2016, 131258, 'Silindripae tihendid 2 tk', 'OEM', 85, 'Remont', '-\r'),
	(68, 2016, 131258, 'Õlivahetus filtriga', 'Motul 10W-40 mineraal', 30, 'Hooldus', '-\r'),
	(69, 2016, 131258, 'Õhufilter', 'Hiflo', 10, 'Hooldus', '-\r'),
	(70, 2016, 131258, 'Esimesed piduriklotsid', 'Ferodo Platinum', 25, 'Hooldus', '-\r'),
	(71, 2016, 131258, 'Küünlad', 'NGK', 7, 'Hooldus', '-\r'),
	(72, 2016, 131258, 'UUS kompressioon külmalt 10,3 bar (150 psi)!', '-', 0, 'Näit', '-\r'),
	(73, 2016, 132300, 'Kompressioon külmalt! 145 psi', '-', 0, 'Näit', '-\r'),
	(74, 2016, 132300, 'Õlivahetus filtriga', 'Motul 10W-40 mineraal', 30, 'Hooldus', '-\r'),
	(75, 2016, 132300, 'Mootori poldid üle pingutatud', '-', 0, 'Hooldus', '-\r'),
	(76, 2016, 132300, 'Klapivahede reguleerimine', '-', 0, 'Hooldus', '-\r'),
	(77, 2016, 134500, 'Siduri töösilinder', 'OEM', 121, 'Remont', '-\r'),
	(78, 2016, 135100, 'Õlivahetus filtriga', 'Motul 20W-50', 38, 'Hooldus', '-\r'),
	(79, 2016, 135100, 'Õhufilter', 'Hiflo HF163', 10, 'Hooldus', '-\r'),
	(80, 2016, 137715, 'Lenkstang (2.hand Saksast)', 'OEM', 100, 'Remont', '-\r'),
	(81, 2016, 137715, 'Sidurivoolik', 'Probrake', 40, 'Remont', '-\r'),
	(82, 2016, 137715, 'Pivot laager (käigukast vasak)', 'OEM', 40, 'Remont', '-\r'),
	(83, 2016, 137715, 'Kiige polt (Pivot pin) 2tk', 'OEM', 49, 'Remont', '-\r'),
	(84, 2016, 138760, 'Tagarehv ', 'Metzeler Anakee Wild', 130, 'Hooldus', '-\r'),
	(85, 2016, 139080, 'Õlivahetus filtriga', 'Motul 20W-50', 38, 'Hooldus', '-\r'),
	(86, 2016, 140000, 'Õlivahetus', 'Motul 10W-40', 27, 'Hooldus', '-\r'),
	(87, 2016, 140000, 'Käigukasti laagrid 6tk', 'SKF', 33, 'Remont', '-\r'),
	(88, 2016, 140000, 'Käigukasti simmerid 5 tk', 'OEM', 76, 'Remont', '-\r'),
	(89, 2016, 140000, 'Ülekande õlid', 'Liqui Moly 75W-90', 30, 'Hooldus', '-\r'),
	(90, 2017, 142000, 'Esimene pivot joint', 'OEM. 2 hand', 45, 'Remont', '-\r'),
	(91, 2017, 142000, 'Uus pivoti kumm', 'Ebay', 15, 'Remont', '-\r'),
	(92, 2017, 142000, 'Esiamordi tolmukaitsed 2 tk', 'OEM', 29, 'Remont', '-\r'),
	(93, 2017, 142175, 'Esirehv', 'Mitas E-07 Dakar', 81, 'Hooldus', 'keskel 5mm servas 6mm\r'),
	(94, 2017, 143000, 'Esiamordi simmerling parem', 'OEM', 30, 'Remont', '-\r'),
	(95, 2017, 143000, 'Starteri bendixi vahetus', 'Aleklena OÜ', 45, 'Remont', '-\r'),
	(96, 2017, 144408, 'Õlivahetus filtriga', 'Motul 20W-50', 38, 'Hooldus', '-\r'),
	(97, 2017, 145000, 'Tagarehv', ' Conti Trail AttackPoolkulund, vahetus', 14, 'Hooldus', '-\r'),
	(98, 2017, 145000, 'Kardaaniõli', 'Castrol 75W-90', 7, 'Hooldus', '-\r'),
	(99, 2017, 146000, 'pivot laagrite asemele Nylager Nushingud', 'mouthfulloflake', 51, 'Remont', '-\r'),
	(100, 2017, 147500, 'Tagarehv', 'Avon Trekrider', 131, 'Hooldus', 'Soojaga muutus kumm väga pehmeks. Hakkas sirgel ujuma ning kurvides oli ebakindel.\r'),
	(101, 2017, 149574, 'Kompressioon P- 10,8 bar V- 10,6 bar', '-', 0, 'Näit', '-\r'),
	(102, 2017, 149574, 'Kardaanivõll (kasutatud Lätist)', 'OEM', 45, 'Remont', '-\r'),
	(103, 2017, 149574, 'Kardaanilaagrite kontroll', '-', 0, 'Hooldus', '-\r'),
	(104, 2017, 149574, 'Õlivahetus filtriga', 'Motul 10W-40', 27, 'Hooldus', '-\r'),
	(105, 2017, 149574, 'Kardaaniõli', 'Motul 75W-140', 7, 'Hooldus', '-\r'),
	(106, 2017, 149574, 'Armatuuritulede vahetus', 'Ebay', 5, 'Hooldus', '-\r'),
	(107, 2017, 149574, 'Küljejala ja harkjala värvimine/ määrimine', '-', 40, 'Remont', '-\r'),
	(108, 2017, 149574, 'Kütusefilter', 'OEM', 20, 'Hooldus', '-\r'),
	(109, 2018, 152000, 'Õlivahetus filtriga', 'Motul 20W-50', 34, 'Hooldus', '-\r'),
	(110, 2018, 156300, 'Tagarehv', 'Metzeler Tourance', 133, 'Hooldus', '-\r'),
	(111, 2018, 157000, 'Õlivahetus ilma filtrita', 'Motul 20W-50', 43, 'Hooldus', '-\r'),
	(112, 2018, 157000, 'Uus Geneka rihm', 'Conti 4 PK 611', 7, 'Hooldus', '-\r'),
	(113, 2018, 160700, 'Käigukasti õlivahetus', 'Castrol 75W-140', 16, 'Hooldus', '-\r'),
	(114, 2018, 160700, 'UUS TAGAAMORT', 'Hyperpro', 430, 'Uuendus', '-\r'),
	(115, 2018, 160700, 'UUS esiamordi vedru', 'Hyperpro', 114, 'Uuendus', '-\r'),
	(116, 2019, 161269, 'Kompressioon V 10,2 P 10,3 temp 10C', '-', 0, 'Näit', '-\r'),
	(117, 2019, 161269, 'Pistikute hooldus', '-', 0, 'Hooldus', '-\r'),
	(118, 2019, 161269, 'Käimari nuudid määritud', '-', 0, 'Hooldus', '-\r'),
	(119, 2019, 161269, 'Klapivahede kontroll', '-', 0, 'Hooldus', '-\r'),
	(120, 2019, 161269, 'Tagaraami tugevdused saba alla', 'Kustom', 0, 'Remont', '-\r'),
	(121, 2019, 161269, 'Küünlad', 'Denso K22TNR-S', 9, 'Hooldus', '-\r'),
	(122, 2019, 161269, 'Õhufilter', 'Hiflo HFA 7910', 9, 'Hooldus', '-\r'),
	(123, 2019, 161269, 'Õlivahetus filtriga', 'Elf 20w-50', 35, 'Hooldus', '-\r'),
	(124, 2019, 161269, 'Kardaaniõli', '85W-90', 5, 'Hooldus', '-\r'),
	(125, 2019, 161269, 'Migsel GPS hoidja', 'Migsel', 60, 'Uuendus', '-\r'),
	(126, 2019, 161269, 'Klapikaane väikesed tihendid', 'OEM', 15, 'Remont', '-\r'),
	(127, 2019, 161269, 'Tagatuli', 'Ebay', 10, 'Remont', '-\r'),
	(128, 2019, 161269, 'Piduriklotsid tagumised', 'TRW', 25, 'Hooldus', '-\r'),
	(129, 2019, 161269, 'Esirehv', 'Mitas E-07 Dakar', 75, 'Hooldus', '-\r'),
	(130, 2019, 165000, 'Õlivahetus filtrita', 'Motul 20W-50', 30, 'Hooldus', '-\r'),
	(135, 2020, 170000, 'q', 'q', 500, 'q', 'q');
/*!40000 ALTER TABLE `sisendinfo` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
